-- CREACIÓN DEL USUARIO DE LA BASE DE DATOS
-- CREATE USER edcamp PASSWORD 'Sup3rS3gur4+';

-- CREACIÓN DE LA BASE DE DATOS
-- CREATE DATABASE edcamp OWNER edcamp;

-- Roles: perfiles del sistema
CREATE TABLE roles (
    id SERIAL NOT NULL,
    name VARCHAR(50) NOT NULL,
    active BOOLEAN NOT NULL DEFAULT TRUE,
    created_at TIMESTAMP NOT NULL DEFAULT now(),
    updated_at TIMESTAMP,
    CONSTRAINT roles_id_pk PRIMARY KEY (id),
    CONSTRAINT roles_name_uk UNIQUE (name)
);

COMMENT ON TABLE roles IS 'Perfiles de los usuarios del sistema';

-- USERS usuarios del sistema
CREATE TABLE users (
    id SERIAL NOT NULL,
    role_id INT NOT NULL,
    name VARCHAR(100) NOT NULL,
    email VARCHAR(255) NOT NULL,
    picture VARCHAR(255),
    password VARCHAR(255) NOT NULL,
    remember_token VARCHAR(100),
    created_at TIMESTAMP NOT NULL DEFAULT now(),
    updated_at TIMESTAMP,
    CONSTRAINT users_id_pk PRIMARY KEY (id),
    CONSTRAINT users_email_uk UNIQUE (email),
    CONSTRAINT users_role_id_fk FOREIGN KEY (role_id) REFERENCES roles (id) ON UPDATE RESTRICT ON DELETE RESTRICT
);

COMMENT ON TABLE users IS 'Usuarios del sistema';

-- Password_Resets Recuperación de contraseñas
CREATE TABLE passwords_resets (
    email VARCHAR(255) NOT NULL,
    token VARCHAR(255) NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT now(),
    CONSTRAINT passwords_resets_email_fk FOREIGN KEY (email) REFERENCES users (email) ON UPDATE NO ACTION ON DELETE NO ACTION
);

COMMENT ON TABLE passwords_resets IS 'Permite recuperar la contraseña del usuario';

-- series productos para la venta
CREATE TABLE series (
    id SERIAL NOT NULL,
    name VARCHAR(100) NOT NULL,
    picture VARCHAR(1024),
    information TEXT NOT NULL,
    price NUMERIC(7,2) NOT NULL,
    trailer_url VARCHAR(1024) NOT NULL,
    user_id INT NOT NULL, -- Usuario propietario de la serie
    created_at TIMESTAMP NOT NULL DEFAULT now(),
    updated_at TIMESTAMP,
    CONSTRAINT series_id_pk PRIMARY KEY (id),
    CONSTRAINT series_user_id_fk FOREIGN KEY (user_id) REFERENCES users (id) ON UPDATE RESTRICT ON DELETE RESTRICT
);

COMMENT ON TABLE series IS 'Series a vender';

-- Chapters capítulos de las series a vender
CREATE TABLE chapters (
    id SERIAL NOT NULL,
    serie_id INT NOT NULL,
    name VARCHAR(100) NOT NULL,
    chapter_url VARCHAR(1024) NOT NULL,
    duration TIME NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMP,
    CONSTRAINT chapters_id_pk PRIMARY KEY (id),
    CONSTRAINT chapters_serie_id_fk FOREIGN KEY (serie_id) REFERENCES series (id) ON UPDATE RESTRICT ON DELETE CASCADE
);

COMMENT ON TABLE chapters IS 'Almacena los capítulos y su respectiva información';

-- Invoices Facturación
CREATE TABLE invoices (
    id SERIAL NOT NULL,
    invoice_date DATE NOT NULL DEFAULT now(),
    user_id INT NOT NULL,
    serie_id INT NOT NULL,
    state VARCHAR(25) NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT now(),
    updated_at TIMESTAMP,
    CONSTRAINT invoices_id_pk PRIMARY KEY (id),
    CONSTRAINT invoices_user_id_fk FOREIGN KEY (user_id) REFERENCES users (id) ON UPDATE RESTRICT ON DELETE RESTRICT,
    CONSTRAINT invoices_serie_id_fk FOREIGN KEY (serie_id) REFERENCES series (id) ON UPDATE RESTRICT ON DELETE RESTRICT
);

COMMENT ON TABLE invoices IS 'Facturación';

-- TAGS tabla de los tag generales
CREATE TABLE tags (
    id SERIAL NOT NULL,
    name VARCHAR(50) NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMP,
    CONSTRAINT tags_id_pk PRIMARY KEY (id),
    CONSTRAINT tags_name_uk UNIQUE (name)
);

COMMENT ON TABLE tags IS 'Tags de las series';

-- Serie_Tag tabla pivote que permite almacenar los tag en cada serie
CREATE TABLE serie_tag (
    id SERIAL NOT NULL,
    serie_id INT NOT NULL,
    tag_id INT NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMP,
    CONSTRAINT serie_tag_id_pk PRIMARY KEY (id),
    CONSTRAINT serie_tag_serie_id_fk FOREIGN KEY (serie_id) REFERENCES series (id) ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT serie_tag_tag_id_fk FOREIGN KEY (tag_id) REFERENCES tags (id) ON UPDATE CASCADE ON DELETE CASCADE
);

COMMENT ON TABLE serie_tag IS 'Almacena los tags de cada serie';
