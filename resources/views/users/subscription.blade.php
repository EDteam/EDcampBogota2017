@extends('layouts.master')

@section('title')
    Suscripción
@endsection

@section('header')
    @include('partials.header')
    @include('partials.user_menu')
@endsection

@section('content')
    <main class="Subscription  u-afterFixed">
        @if(isset($status))
            <div class="Form-message  u-success">
                {{ $status }}
            </div>
        @endif
        <header class="u-flex-space-between">
            <h2>Suscribirse</h2>
            <span class="u-text-right">
                <a href="#" class="u-button u-bg-white u-alert  u-border-alert">{{ $price->price }} <sup>USD</sup> <b>/mes</b></a>
            </span>
        </header>
        <div class="PayU">
            {!! Form::open(['route' => 'subscriptions.payu']) !!}
                <div class="">
                    Cantidad de meses:&nbsp;
                    {!! Form::number('months', 1, [
                        'id' => 'months',
                        'step' => 1,
                        'min' => 1,
                        'style' => 'width:50px;',
                        'required'
                    ]) !!}
                </div>
                <div class="">
                    Monto a pagar:&nbsp;
                    <b id="price">{{ $price->price }}</b> <b>USD</b>
                </div>
                <div class="Form-element  u-bg-success  u-lg-w35">
                    {!! Form::submit('Enviar') !!}
                </div>
            {!! Form::close() !!}
        </div>
        <script>
            const months = document.getElementById('months'),
                price = document.getElementById('price'),
                priceVal = Number(price.innerText)

            months.addEventListener('change', e => {
                amount = priceVal * months.value
                price.innerText = amount
            })
        </script>
    </main>
@endsection
