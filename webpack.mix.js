const { mix } = require('laravel-mix'),
  BrowserSyncPlugin = require('browser-sync-webpack-plugin')

mix.webpackConfig({
  plugins: [
    new BrowserSyncPlugin({
      port: 20171,
      proxy: 'edcamp.local',
      files: [
        '**/*.scss',
        '**/*.php'
      ]
    }, {reload: true})
  ]
});

mix
  .js('resources/assets/app.js', 'public/js')
  .sass('resources/assets/app.scss', 'public/css')
  .disableNotifications();
